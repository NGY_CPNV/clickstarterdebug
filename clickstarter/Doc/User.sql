SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema ClickStarter
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ClickStarter` DEFAULT CHARACTER SET utf8 ;

-- -----------------------------------------------------
-- Table `ClickStarter`.`tbl_Countries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ClickStarter`.`tbl_Countries` (
  `id_Country` INT NOT NULL AUTO_INCREMENT COMMENT 'Country ID',
  `Name_Country` VARCHAR(45) NOT NULL COMMENT 'Country name',
  PRIMARY KEY (`id_Country`)) COMMENT 'Table which contains available Countries'
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ClickStarter`.`tbl_Cities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ClickStarter`.`tbl_Cities` (
  `id_City` INT NOT NULL AUTO_INCREMENT COMMENT 'City ID',
  `Name_City` VARCHAR(45) NOT NULL COMMENT 'City name',
  `pc_City` VARCHAR(45) NOT NULL COMMENT 'City postal code',
  `fk_Country` INT NOT NULL COMMENT 'The Country where the City is',
  PRIMARY KEY (`id_City`),
  INDEX `fk_tbl_Cities_tbl_Country1_idx` (`fk_Country` ASC),
  CONSTRAINT `fk_tbl_Cities_tbl_Country1`
    FOREIGN KEY (`fk_Country`)
    REFERENCES `ClickStarter`.`tbl_Country` (`id_Country`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION) COMMENT 'Table which contains the availabe Countries'
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ClickStarter`.`tbl_Members`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ClickStarter`.`tbl_Members` (
  `id_Members` INT NOT NULL AUTO_INCREMENT COMMENT 'Member ID',
  `fk_City` INT NOT NULL COMMENT 'City where the Member lives',
  `last_name_Member` VARCHAR(45) NOT NULL COMMENT 'Member last name',
  `first_name_Member` VARCHAR(45) NOT NULL COMMENT 'Member first name',
  `psw_Member` VARCHAR(255) NOT NULL COMMENT 'Member password',
  `mail_Member` VARCHAR(45) NOT NULL COMMENT 'Member mail',
  `registerDate_Member` DATE NOT NULL COMMENT 'Member register date',
  PRIMARY KEY (`id_Members`),
  INDEX `fk_tbl_Members_tbl_Cities_idx` (`fk_City` ASC),
  CONSTRAINT `fk_tbl_Members_tbl_Cities`
    FOREIGN KEY (`fk_City`)
    REFERENCES `ClickStarter`.`tbl_Cities` (`id_City`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)  COMMENT 'Table which contains Members'
ENGINE = InnoDB;

USE ClickStarter;

INSERT INTO tbl_Countries (Name_Country) VALUES
('Suisse'),
('France'),
('Allemagne'),
('Italie'),
('Autriche'),
('Belgique');

INSERT INTO tbl_Cities (Name_City, pc_City, fk_Country) VALUES
('Berne',3001,1),
('Zurich',8000,1),
('Paris',75000,2),
('Maseille',13001,2),
('Berlin',10115,3),
('Munich',80331,3),
('Rome',00100,4),
('Venise',30100,4),
('Vienne',38200,5),
('Graz',8010,5),
('Bruges',33520,6),
('Gand',9000,6);