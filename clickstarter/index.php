<?php 
session_start();

define('ROOT_URL',    "http://".$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['PHP_SELF']), '/.\\'));
define('DIR_BASE',    dirname( __FILE__ ));

$pages = include(DIR_BASE . "/src/config/pages.php");

$page = 'home';
if(isset($_GET["page"]) && $_GET["page"] != ''){
  $page = htmlspecialchars($_GET["page"]);
}

$flashMessage = "";
$script = DIR_BASE . "/src/scripts/".$page.".php";
if(file_exists($script)){
  try{
    include($script);
  }
  catch(Exception $e){
    $flashMessage = $e->getMessage();
  }
}


if(!(file_exists(DIR_BASE . "/src/pages/".$page.".php"))){
  $page = 'error';
  $code = '404';
  header($_SERVER["SERVER_PROTOCOL"]." ".$code);
}
$title = isset($pages[$page]["title"]) ? $pages[$page]["title"] : '';
$keywords = isset($pages[$page]["keywords"]) ? $pages[$page]["keywords"] : '';
$description = isset($pages[$page]["description"]) ? $pages[$page]["description"] : '';

include(DIR_BASE . "/src/layout.php");
