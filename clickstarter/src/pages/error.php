<div class="gtco-cover gtco-cover-sm" style="background-image:url(<?= ROOT_URL;?>/assets/images/img_bg_3.jpg);">
    <div class="overlay"></div>
    <div class="gtco-container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <div class="display-t">
                    <div class="display-tc animate-box" data-animate-effect="fadeIn">
                        <h1>Erreur</h1>
                        <h2>Une erreur est survenue lors du chargement de la page</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

