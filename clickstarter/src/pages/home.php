    <div class="gtco-container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <div class="display-t">
                    <div class="display-tc animate-box" data-animate-effect="fadeIn">
                        <h1>ClickStarter</h1>
                        <h2>La où les projets prennent vie</h2>
                        <p><a href="#" class="btn btn-default">Get Started</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="gtco-counter" class="gtco-bg gtco-counter">
    <div class="gtco-container">
        <div class="row">
            <div class="display-t">
                <div class="display-tc">
                    <div class="col-md-3 col-sm-6 animate-box">
                        <div class="feature-center">
                            <span class="icon">
                                <i class="icon-eye"></i>
                            </span>
                            <span class="counter js-counter" data-from="0" data-to="22070" data-speed="5000" data-refresh-interval="50" style="color:deepskyblue">1</span>
                            <span class="counter-label" style="color:deepskyblue">Creativity Fuel</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 animate-box">
                        <div class="feature-center">
                            <span class="icon">
                                <i class="icon-anchor"></i>
                            </span>

                            <span class="counter js-counter" data-from="0" data-to="97" data-speed="5000" data-refresh-interval="50">1</span>
                            <span class="counter-label">Happy Clients</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 animate-box">
                        <div class="feature-center">
                            <span class="icon">
                                <i class="icon-briefcase"></i>
                            </span>
                            <span class="counter js-counter" data-from="0" data-to="402" data-speed="5000" data-refresh-interval="50">1</span>
                            <span class="counter-label">Projects Done</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 animate-box">
                        <div class="feature-center">
                            <span class="icon">
                                <i class="icon-clock"></i>
                            </span>

                            <span class="counter js-counter" data-from="0" data-to="212023" data-speed="5000" data-refresh-interval="50">1</span>
                            <span class="counter-label">Hours Spent</span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="gtco-features">
    <div class="gtco-container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i class="icon-eye"></i>
						</span>
                    <h3>Retina Ready</h3>
                    <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
                    <p><a href="#" class="btn btn-primary">Learn More</a></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i class="icon-command"></i>
						</span>
                    <h3>Fully Responsive</h3>
                    <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
                    <p><a href="#" class="btn btn-primary">Learn More</a></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<i class="icon-power"></i>
						</span>
                    <h3>Web Starter</h3>
                    <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
                    <p><a href="#" class="btn btn-primary">Learn More</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="gtco-features-2">
    <div class="gtco-container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center gtco-heading">
                <h2>Why Choose Us</h2>
                <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-check"></i>
						</span>
                    <div class="feature-copy">
                        <h3>Retina Ready</h3>
                        <p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
                    </div>
                </div>

                <div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-check"></i>
						</span>
                    <div class="feature-copy">
                        <h3>Fully Responsive</h3>
                        <p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
                    </div>
                </div>

                <div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-check"></i>
						</span>
                    <div class="feature-copy">
                        <h3>Ready To Use</h3>
                        <p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
                    </div>
                </div>

            </div>

            <div class="col-md-6">
                <div class="gtco-video gtco-bg" style="background-image: url(<?= ROOT_URL;?>/assets/images/img_1.jpg); ">
                    <a href="https://vimeo.com/channels/staffpicks/93951774" class="popup-vimeo"><i class="icon-video2"></i></a>
                    <div class="overlay"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="gtco-testimonial">
    <div class="gtco-container">
        <!-- <div class="row"> -->
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center gtco-heading">
                <h2>Testimonial</h2>
            </div>
        </div>
        <div class="row animate-box">


            <div class="owl-carousel owl-carousel-fullwidth ">
                <div class="item">
                    <div class="testimony-slide active text-center">
                        <figure>
                            <img src="<?= ROOT_URL;?>/assets/images/person_1.jpg" alt="user">
                        </figure>
                        <span>Jean Doe, via <a href="#" class="twitter">Twitter</a></span>
                        <blockquote>
                            <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>
                <div class="item">
                    <div class="testimony-slide active text-center">
                        <figure>
                            <img src="<?= ROOT_URL;?>/assets/images/person_2.jpg" alt="user">
                        </figure>
                        <span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
                        <blockquote>
                            <p>&ldquo;Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>
                <div class="item">
                    <div class="testimony-slide active text-center">
                        <figure>
                            <img src="<?= ROOT_URL;?>/assets/images/person_3.jpg" alt="user">
                        </figure>
                        <span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
                        <blockquote>
                            <p>&ldquo;Far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
        <!-- </div> -->
    </div>
</div>



<div id="gtco-started">
    <div class="gtco-container">
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center gtco-heading">
                <h2>Get Started</h2>
            </div>
        </div>
        <div class="row animate-box">
            <div class="col-md-12">
                <form class="form-inline">
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <label for="email" class="sr-only">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <label for="password" class="sr-only">Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <button type="submit" class="btn btn-default btn-block">Sign Up</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>