<div class="gtco-section">
    <div class="gtco-container">
        <div class="row">
            <div class="col-md-12 animate-box">
                <h3>Inscription</h3>
                <?php if($flashMessage != ""): ?>
                    <div class="alert alert-warning"><?= $flashMessage;?></div>
                <?php endif; ?>
                <form method="POST" action="index.php?page=signup" role="form" data-toggle="validator">
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label for="flast">Nom</label>
                            <input type="text" id="flastname" name="flastname" class="form-control" placeholder="Votre nom" required>
                        </div>
                        <div class="col-md-6">
                            <label for="ffirst">Prénom</label>
                            <input type="text" id="ffirstname" name="ffirstname" class="form-control" placeholder="Votre Prénom" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label for="femail">E-mail</label>
                            <input type="email" id="femail" name="femail" class="form-control" placeholder="Votre adresse email" required>
                        </div>

                        <div class="col-sm-6">
                            <label for="femailconf">Confirmation</label>
                            <input type="email" id="femailconf" name="femailconf" class="form-control" placeholder="Votre adresse email" required>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label for="fpassword" class="control-label">Mot de passe</label>
                            <input type="password" data-minlength="8" class="form-control" id="fpassword" name="fpassword" placeholder="Mot de passe" required>
                            <div class="help-block with-errors">Minimum 8 caractères</div>
                        </div>
                        <div class="col-sm-6">
                            <label for="fpasswordConfirm" class="control-label">Confirmation</label>
                            <input type="password" class="form-control" id="fpasswordConfirm" name="fpasswordConfirm" data-match="#fpassword" data-match-error="Les mots de passes ne correspondent pas" placeholder="Votre mot de passe" required>
                            <div class="help-block"></div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-6">
                            <a href="">Mot de passe oublié ?</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Créer un compte" class="btn btn-primary">
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>



