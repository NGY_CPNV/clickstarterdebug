<div class="gtco-section">
    <div class="gtco-container">
        <div class="row">
            <div class="col-md-12 animate-box">
                <h3>Connexion</h3>
                <?php if($flashMessage != ""): ?>
                    <div class="alert alert-warning"><?= $flashMessage;?></div>
                <?php endif; ?>
                <form method="POST" action="index.php?page=login" role="form" data-toggle="validator">
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label for="femail">E-mail</label>
                            <input type="text" id="femail" name="femail" class="form-control" placeholder="Votre e-mail">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label for="fpassword">Mot de passe</label>
                            <input type="password" id="fpassword" name="fpassword" class="form-control" placeholder="Votre mot de passe">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <a href="">Mot de passe oublié ?</a>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <a href="index.php?page=signup">Première visite sur ClickStarter ? Je m'inscris !</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Connexion" class="btn btn-primary">
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>