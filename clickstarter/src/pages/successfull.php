<div class="gtco-cover gtco-cover-sm" style="background-image:url(<?= ROOT_URL;?>/assets/images/img_bg_3.jpg);">
    <div class="overlay"></div>
    <div class="gtco-container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <div class="display-t">
                    <div class="display-tc animate-box" data-animate-effect="fadeIn">
                        <h1><?=$msg1 ?></h1>
                        <h2><?=$msg2 ?></h2>
                        <h2>Vous allez être redirigé dans
                            <b id="compteRebour_affiche"></b>
                            <script type="text/javascript">rebour(5);</script>
                        sur la page d'accueil</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
