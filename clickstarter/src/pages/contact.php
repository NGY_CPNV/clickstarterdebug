<div class="gtco-section">
    <div class="gtco-container">
        <div class="row">
            <div class="col-md-6 animate-box">
                <h3>Contactez nous</h3>
                <form action="#">
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label for="fname">First Name</label>
                            <input type="text" id="fname" class="form-control" placeholder="Your firstname">
                        </div>
                        <div class="col-md-6">
                            <label for="lname">Last Name</label>
                            <input type="text" id="lname" class="form-control" placeholder="Your lastname">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="email">Email</label>
                            <input type="text" id="email" class="form-control" placeholder="Your email address">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="subject">Subject</label>
                            <input type="text" id="subject" class="form-control" placeholder="Your subject of this message">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="message">Message</label>
                            <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Write us something"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Send Message" class="btn btn-primary">
                    </div>

                </form>
            </div>
            <div class="col-md-5 col-md-push-1 animate-box">

                <div class="gtco-contact-info">
                    <h3>Contact Information</h3>
                    <ul>
                        <li class="address">Avenue de la Gare 14<br> 1450 Sainte-Croix</li>
                        <li class="phone"><a href="tel://1234567920">+ 1235 2355 98</a></li>
                        <li class="email"><a href="mailto:info@clickstarter.com">info@clickstarter.com</a></li>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2730.1263144568643!2d6.497879715820485!3d46.82151364987131!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478db876b925cce3%3A0xc43383adf22945c2!2sCentre+professionnel+du+Nord+Vaudois+(ETSC-CPNV)!5e0!3m2!1sfr!2sch!4v1512040045962" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </ul>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="gtco-cover gtco-cover-sm" style="background-image:url(<?= ROOT_URL;?>/assets/images/img_bg_3.jpg);">
    <div class="overlay"></div>
    <div class="gtco-container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <div class="display-t">
                    <div class="display-tc animate-box" data-animate-effect="fadeIn">
                        <h1>Keep it simple</h1>
                        <h2>Free html5 templates Made by <a href="http://gettemplates.co" target="_blank">gettemplates.co</a></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
