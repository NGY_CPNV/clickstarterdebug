<div id="gtco-services">
    <div class="gtco-container">

        <div class="row animate-box">
            <div class="col-md-8 text-center gtco-heading" style="margin-bottom: 0">
                <h2 style="text-align:left">Paramètres</h2>
            </div>
        </div>

        <div class="row animate-box">

            <div class="gtco-tabs">
                <ul class="gtco-tab-nav" style="margin-bottom: 0">
                    <li class="active"><a href="#" data-tab="1"><span class="icon visible-xs"><i class="icon-paper"></i></span><span class="hidden-xs">Mon Compte</span></a></li>
                    <li><a href="#" data-tab="2"><span class="icon visible-xs"><i class="icon-bag"></i></span><span class="hidden-xs">Modifier mon profil</span></a></li>
                    <li><a href="#" data-tab="3"><span class="icon visible-xs"><i class="icon-speech-bubble"></i></span><span class="hidden-xs">Notifications</span></a></li>
                    <li><a href="#" data-tab="4"><span class="icon visible-xs"><i class="icon-box"></i></span><span class="hidden-xs">Mes contributions</span></a></li>
                </ul>

                <!-- Tabs -->
                <div class="gtco-tab-content-wrap" style="top:10px">
                    <div class="gtco-tab-content tab-content active" data-tab-content="1">
                        <div class="col-md-12">
                            <?php if($flashMessage != ""): ?>
                                <div class="alert alert-warning"><?= $flashMessage;?></div>
                            <?php endif; ?>
                            <form method="POST" action="index.php?page=account" role="form" data-toggle="validator">
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label for="fpassword">Changer de mot de passe</label>
                                        <input type="password"  data-minlength="8" id="fpassword" name="fpassword" class="form-control" placeholder="Votre mot de passe actuel" style="font-size: 16px">
                                        <div class="help-block with-errors">Minimum 8 caractères</div>
                                    </div>
                                    <div class="col-md-3" style="margin-top: 32px">
                                        <input type="password" id="fnewpassword" name="fnewpassword" data-match="#fpassword" data-match-error="Les mots de passes ne correspondent pas" class="form-control" placeholder="Votre nouveau mot de passe" style="font-size: 16px">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-6">
                                        <a href="">Mot de passe oublié ?</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Enregistrer" class="btn btn-primary">
                                </div>
                            </form>

                        </div>
                    </div>


                    <div class="gtco-tab-content tab-content" data-tab-content="2">
                        <div class="col-md-6">
                            <div class="icon icon-xlg">
                                <i class="icon-bar-graph"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h2>Online Marketing</h2>
                            <p>Paragraph placeat quis fugiat provident veritatis quia iure a debitis adipisci dignissimos consectetur magni quas eius nobis reprehenderit soluta eligendi quo reiciendis fugit? Veritatis tenetur odio delectus quibusdam officiis est.</p>

                            <p>Ullam dolorum iure dolore dicta fuga ipsa velit veritatis molestias totam fugiat soluta accusantium omnis quod similique placeat at. Dolorum ducimus libero fuga molestiae asperiores obcaecati corporis sint illo facilis.</p>

                            <div class="row">
                                <div class="col-md-6">
                                    <h2 class="uppercase">Ready to use</h2>
                                    <p>Ullam dolorum iure dolore dicta fuga ipsa velit veritatis</p>
                                </div>
                                <div class="col-md-6">
                                    <h2 class="uppercase">100% Satisfaction</h2>
                                    <p>Ullam dolorum iure dolore dicta fuga ipsa velit veritatis</p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="gtco-tab-content tab-content" data-tab-content="3">
                        <div class="col-md-6">
                            <h2>e-Commerce</h2>
                            <p>Ullam dolorum iure dolore dicta fuga ipsa velit veritatis molestias totam fugiat soluta accusantium omnis quod similique placeat at. Dolorum ducimus libero fuga molestiae asperiores obcaecati corporis sint illo facilis.</p>

                            <p>Paragraph placeat quis fugiat provident veritatis quia iure a debitis adipisci dignissimos consectetur magni quas eius nobis reprehenderit soluta eligendi quo reiciendis fugit? Veritatis tenetur odio delectus quibusdam officiis est.</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <h2 class="uppercase">Easy to shop</h2>
                                    <p>Ullam dolorum iure dolore dicta fuga ipsa velit veritatis</p>
                                </div>
                                <div class="col-md-6">
                                    <h2 class="uppercase">No credit card required</h2>
                                    <p>Ullam dolorum iure dolore dicta fuga ipsa velit veritatis</p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="gtco-tab-content tab-content" data-tab-content="4">
                        <div class="col-md-6">
                            <div class="icon icon-xlg">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h2>Logo &amp; Branding</h2>
                            <p>Paragraph placeat quis fugiat provident veritatis quia iure a debitis adipisci dignissimos consectetur magni quas eius nobis reprehenderit soluta eligendi quo reiciendis fugit? Veritatis tenetur odio delectus quibusdam officiis est.</p>

                            <p>Ullam dolorum iure dolore dicta fuga ipsa velit veritatis molestias totam fugiat soluta accusantium omnis quod similique placeat at. Dolorum ducimus libero fuga molestiae asperiores obcaecati corporis sint illo facilis.</p>

                            <div class="row">
                                <div class="col-md-6">
                                    <h2 class="uppercase">Pixel perfect</h2>
                                    <p>Ullam dolorum iure dolore dicta fuga ipsa velit veritatis</p>
                                </div>
                                <div class="col-md-6">
                                    <h2 class="uppercase">User Interface Expert</h2>
                                    <p>Ullam dolorum iure dolore dicta fuga ipsa velit veritatis</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

