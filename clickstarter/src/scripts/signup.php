<?php

include DIR_BASE ."/src/includes/DbManagement.php";

// If signup form is send
if($_SERVER['REQUEST_METHOD'] == 'POST') {

    extract($_POST);
    //If passwords are the same, calls CreateUser()
    if($fpassword == $fpasswordConfirm)
    {
        if(Get_Mail($femail) == NULL)
        {
            CreateUser($ffirstname, $flastname, $fpassword, $femail);
            header('Location: index.php?page=successfull&msg=1');
        }
        $flashMessage = 'Cette adresse mail existe déjà';

    }
}