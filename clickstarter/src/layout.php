<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ClickStarter</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="gettemplates.co" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?= ROOT_URL;?>/assets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?= ROOT_URL;?>/assets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?= ROOT_URL;?>/assets/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?= ROOT_URL;?>/assets/css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?= ROOT_URL;?>/assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?= ROOT_URL;?>/assets/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="<?= ROOT_URL;?>/assets/css/style.css">

	<!-- Modernizr JS -->
	<script src="<?= ROOT_URL;?>/assets/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="<?= ROOT_URL;?>/assets/js/respond.min.js"></script>
	<![endif]-->

    <!-- Timer -->
    <script src="<?= ROOT_URL;?>/assets/js/timecount.js"></script>

	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">
        <nav class="gtco-nav" role="navigation">
            <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 text-left">
                <div id="gtco-logo"><a href="index.php">ClickStarter</a></div>
            </div>
            <div class="gtco-container">
                <div class="row">
                    <div class="hidden-xs col-sm-4 col-md-4 col-lg-4">
                        <input type="text" class="form-control" placeholder="Rechercher" style="height:30px;padding: 3px;background-color: white">
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <span class="btn-cta"><a href="index.php?page=<?php if(isset($_SESSION['login'])) echo 'logout'; else echo 'login'?>"><span><?php if(isset($_SESSION['login'])) echo 'Déconnexion'; else echo "Se connecter / S'inscrire"?></span></a></span>
                    </div>
                    <div class="text-right menu-2">
                        <?php if(isset($_SESSION['login'])) echo '<a>' . $_SESSION['login']. '</a>' ?>
                    </div>
                </div>
                <div class="row">
                    <div class="hidden-xs col-sm-12 col-md-12 col-lg-12 text-center menu-1">
                        <ul>
                            <li> <a href="index.php">Home</a></li>
                            <li class="has-dropdown">
                                <a href="#">Catégories</a>
                                <ul class="dropdown">
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                </ul>
                            </li>
                            <li><a href="index.php?page=about">À propos</a></li>
                            <li><a href="index.php?page=contact">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

    <header id="gtco-header" class="gtco-cover <?php if ($page == 'home') echo('gtco-cover-sm'); else echo('gtco-cover-xsm')?>" role="banner" style="background-image:url(<?= ROOT_URL;?>/assets/images/img_bg_1.jpg);">
    <?php if($page != 'home') echo '</header>' ?>


        <?php include(DIR_BASE . "/src/pages/".$page.".php");?>

	<footer id="gtco-footer" role="contentinfo">
		<div class="gtco-container">
			<div class="row copyright">
				<div class="col-md-12">
					<p class="pull-left">
						<small class="block">&copy; 2017 ClickStarterTeam. All Rights Reserved.</small>
						<small class="block">Designed by ClickStarterTeam</small>
					</p>
					<p class="pull-right">
						<ul class="gtco-social-icons pull-right">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-linkedin"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
					</p>
				</div>
			</div>
		</div>
	</footer>
    </header>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?= ROOT_URL;?>/assets/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?= ROOT_URL;?>/assets/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?= ROOT_URL;?>/assets/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?= ROOT_URL;?>/assets/js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="<?= ROOT_URL;?>/assets/js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="<?= ROOT_URL;?>/assets/js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="<?= ROOT_URL;?>/assets/js/jquery.magnific-popup.min.js"></script>
	<script src="<?= ROOT_URL;?>/assets/js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="<?= ROOT_URL;?>/assets/js/main.js"></script>
    <!-- validator -->
    <script src="<?= ROOT_URL;?>/assets/js/validator.js"></script>

	</body>
</html>

