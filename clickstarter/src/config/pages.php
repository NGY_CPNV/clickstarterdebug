<?php

return array(
  'home' => array(
    'title'     => 'Accueil',
    'keywords'    => '',
    'description'   => '',
  ),
  'error' => array(
    'title'     => 'Erreur',
    'keywords'    => '',
    'description'   => '',
  ),
  'about' => array(
    'title'     => 'A propos',
    'keywords'    => '',
    'description'   => '',
  ),
  'contact' => array(
    'title'     => 'Contact',
    'keywords'    => '',
    'description'   => '',
  ),
  'login' => array(
    'title'     => 'Login',
    'keywords'    => '',
    'description'   => '',
  ),
  'signup' => array(
    'title'     => 'Créer un compte',
    'keywords'    => '',
    'description'   => '',
  ),
  'successfull' => array(
    'title'     => 'Vous avez bien créé un compte !',
    'keywords'    => '',
    'description'   => '',
  ),
  'account' => array(
    'title'         => 'Gérer mon compte',
    'keywords'      => '',
    'description'   => 'Page de gestion du compte',
  )
);
