<?php

function ConnectDb()
{
    $connection = new PDO("mysql:host=172.17.101.223; dbname=clickstarter", "test", "Passw0rd");
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $connection;
}


function CreateUser($firstname, $lastname, $password, $mail)
{
    $db = ConnectDb();

    $date = date('Y-m-d', time());
    $password = hash("sha256",$password);
    $query = $db->prepare("INSERT INTO tbl_members (fk_City, last_name_Member, first_name_Member, psw_Member, mail_Member, registerDate_Member) VALUES ('1','$lastname','$firstname','$password','$mail','$date')");

    $query->execute();

}

function Get_Mail($mail)
{
    $db = ConnectDb();

    $result = $db->query("SELECT mail_Member FROM tbl_members WHERE mail_Member='".$mail."'");

    $result = $result->fetch();

    return $result;
}

function Login($mail, $password)
{
    $db = ConnectDb();

    $password = hash("sha256",$password);
    $result = $db->query("SELECT * FROM tbl_members WHERE mail_Member='". $mail. "' AND psw_Member='". $password. "'");
    $result = $result->fetch();

    return $result;
}

function Get_user_infos($mail)
{
    $db = ConnectDb();

    $result = $db->query("SELECT * FROM tbl_members WHERE mail_Member='". $mail ."'");
    $result = $result->fetch();

    return $result;
}

function Change_user_password($mail, $password)
{
    $db = ConnectDb();

    $password = hash("sha256",$password);
    $query = $db->prepare("UPDATE tbl_members SET psw_Member='". $password."' WHERE mail_Member='".$mail."'");

    $query->execute();
}